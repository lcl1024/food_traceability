module food_traceability

go 1.13

require (
	github.com/Shopify/sarama v1.26.1 // indirect
	github.com/fsouza/go-dockerclient v1.6.3 // indirect
	github.com/gin-gonic/gin v1.5.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0 // indirect
	github.com/hashicorp/go-version v1.2.0 // indirect
	github.com/hyperledger/fabric v1.4.0
	github.com/hyperledger/fabric-amcl v0.0.0-20200128223036-d1aa2665426a // indirect
	github.com/hyperledger/fabric-sdk-go v1.0.0-beta1
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7 // indirect
	go.uber.org/zap v1.14.0 // indirect
)
