## 牛奶溯源系统组织信息表

机构名称 | 组织标识符 |  组织ID  
-|-|-
奶牛场 | sy_org1 | SyOrg1MSP |
加工厂 | sy_org2 | SyOrg1MSP |
销售终端 | sy_org2 | SyOrg1MSP |

* 每个组织内都有自己的Leader Peer(主节点），Anchor Peer（锚节点），Endorse Peer（背书节点），Committer Peer（确认节点）
* 当然，这只是概念上的划分，可能有些组织内只有一个Peer进程，但是该Peer节点兼任这四个功能
* 就是说，上面的四个角色和真正的Peer进程是**半**多对多的关系，之所以强调是**半**，是因为确认节点和背书节点可以存在多个，但是主节点（此处存疑）和锚节点只能存在一个
    * 主节点：负责获取块，可以有多个主节点
        * Leader peer. When an organization has multiple peers in a channel, a leader peer is a node which takes responsibility for distributing transactions from the orderer to the other committing peers in the organization. A peer can choose to participate in static or dynamic leadership selection.
          It is helpful, therefore to think of two sets of peers from leadership perspective – those that have static leader selection, and those with dynamic leader selection. For the static set, zero or more peers can be configured as leaders. For the dynamic set, one peer will be elected leader by the set. Moreover, in the dynamic set, if a leader peer fails, then the remaining peers will re-elect a leader.
          It means that an organization’s peers can have one or more leaders connected to the ordering service. This can help to improve resilience and scalability in large networks which process high volumes of transactions.
        * 主节点。当一个组织有多个peer在同一个channel通道内时，主节点来负责分配从orderer排序节点获取发送到本组织的确认节点的交易区块。一个节点可以选择参与静态或动态的***主节点选举***。从主节点的角度考虑，两组节点是有帮助的——一组是静态的主节点选举，另一组是动态的主节点选举。对于静态集，可以将0个或多个对等节点设置为**leader**。对于动态集合，仅一个节点会被推选为**leader**，并且在动态集合中，如果leader peer发生故障，那么其余的peer将**重新选举**leader。这意味着一个组织可以有***一个或多个***主节点连接到排序服务。这有助于在处理大量交易的大型网络中提高弹性和可伸缩性。（此处存疑）
    * 锚节点：负责和其他组织通信
        * Anchor peer. If a peer needs to communicate with a peer in another organization, then it can use one of the anchor peers defined in the channel configuration for that organization. An organization can have zero or more anchor peers defined for it, and an anchor peer can help with many different cross-organization communication scenarios.
        * 如果一个节点需要与其他组织中的节点进行通信，它可以使用在该组织的***通道配置***中定义的锚节点。一个组织可以有0个或多个锚节点；锚节点可以用于许多不同的跨组织通信场景。
    * 背书节点：添加了链码的节点
        * Endorsing peer. Every peer with a smart contract can be an endorsing peer if it has a smart contract installed. However, to actually be an endorsing peer, the smart contract on the peer must be used by a client application to generate a digitally signed transaction response. The term endorsing peer is an explicit reference to this fact.
            An endorsement policy for a smart contract identifies the organizations whose peer should digitally sign a generated transaction before it can be accepted onto a committing peer’s copy of the ledger.
        * **具有智能合约的每个对等节点都可以是背书节点**。要真正成为一个背书节点，客户端应用程序必须使用对等节点上的智能合约来生成经过数字签名的交易响应。智能合约的背书策略标识了哪些组织中的节点应该在交易被提交节点的账本副本接受之前对交易进行背书。
    * 确认节点：任何节点都可以是确认节点
        * Committing peer. Every peer node in a channel is a committing peer. It receives blocks of generated transactions, which are subsequently validated before they are committed to the peer node’s copy of the ledger as an append operation.
        * 通道中的每个对等节点都是一个提交节点。它们接收生成的交易区块，随后这些区块在被提交到节点的账本副本之前先被验证。