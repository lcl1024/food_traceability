module food_traceability/test/fabric/application

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0 // indirect
	github.com/hyperledger/fabric-sdk-go v1.0.0-beta1 // indirect
)
