##超级账本链码

# 安装工具
*有两种安装方法*
## 安装证书和区块的生成工具
1. cryptogen证书生成工具
    1. 进入`fabric/common/tools/cryptogen`目录
    2. 执行`go install --tags=nopkcs11`命令
    3. 在`$GOPATH/bin`目录下会生成对应的二进制文件

2. configtxgen区块生成工具
    1. 进入`fabric/common/tools/configtxgen`目录
    2. 执行`go install --tags=nopkcs11`命令
    3. 在`￥GOPATH/bin`目录下会生成对应的二进制文件
    
## 编译fabric源码
1. 获取fabric源码`git clone https://github.com/hyperledger/fabric.git`
(有时因为网络问题可能会clone失败，可以使用码云，`git clone https://gitee.com/mirrors/hyperledger-fabric.git`，该方式clone
下来后记得把**_文件夹名修改为fabric_**)

2. 安装依赖软件
    * `go get github.com/golang/protobuf/protoc-gen-go`
    
3. 在fabric目录下，执行`make release` `make docker`
(MacOS系统需要在Makefile文件中找到第一个GO_LDFLAGS字符串，在行尾加上字符串 -s )

4. 将编译好的文件进行安装
    * Linux系统：在fabric/release/linux-amd64/bin目录下
    * MacOS系统：在fabric/release/darwin-amd64/bin目录下
    1. 将生成的二进制文件赋值到系统文件夹(/usr/local/bin)目录下
    2. 修改每个二进制文件的执行权限`sudo chmod -R 775`
    
    
# 生成配置文件

## 证书配置文件
1. 编写crypto-config.yaml文件  
```yaml
OrdererOrgs:
 - Name: Orderer
   Domain: qklszzngsy.com
   Specs:
     - Hostname: orderer

PeerOrgs:
 - Name: gy1_org1
   Domain: org1.qklszzngsy.com
   Template:
     Count: 3
   Users:
     Count: 4
 - Name: gy1_org2
   Domain: org2.qklszzngsy.com
   Template:
     Count: 3
   Users:
     Count: 4
 - Name: gy1_org3
   Domain: org3.qklszzngsy.com
   Template:
     Count: 3
     Users:
       Count: 4

```
   
2. 生成证书文件  
   `cryptogen generate --config=crypto-config.yaml --output ./crypto-config`
   
## 创世块的生成
1. 编写configtx.yaml文件
```yaml
Organizations:

  - &OrdererOrg

    Name: OrdererOrg
    ID: OrdererMSP
    MSPDir: ./crypto-config/ordererOrganizations/qklszzngsy.com/msp
  
  - &sy_org1
    
    Name: SyOrg1MSP
    ID: SyOrg1MSP
    MSPDir: ./crypto-config/peerOrganizations/org1.qklszzngsy.com/msp
    
    AnchorPeers:
      - Host: peer0.org1.qklszzngyl.com
        Port: 7051

  - &sy_org2

    Name: SyOrg2MSP
    ID: SyOrg2MSP
    MSPDir: ./crypto-config/peerOrganizations/org2.qklszzngsy.com/msp

    AnchorPeers:
      - Host: peer0.org2.qklszzngyl.com
        Port: 7051

  - &sy_org3

    Name: SyOrg3MSP
    ID: SyOrg3MSP
    MSPDir: ./crypto-config/peerOrganizations/org3.qklszzngsy.com/msp

    AnchorPeers:
      - Host: peer0.org3.qklszzngyl.com
        Port: 7051

Orderer: &OrdererDefaults

  OrdererType: solo
  Addresses:
    - orderer.qklszzngyl.com:7050
  BatchTimeout: 2s
  BatchSize:
    MaxMessageCount: 10
    AbsoluteMaxBytes: 98 MB
    PreferredMaxBytes: 521 KB

  Kafka:
    Brokers:
      - 127.0.0.1:9092
  Organizations:

Application: &ApplicationDefaults
  Organizations:

Profiles:

  TestOrgsOrdererGenesis:
    Orderer:
      <<: *OrdererDefaults
      Organizations:
        - *OrdererOrg
    Consortiums:
      SampleConsortium:
        Organizations:
          - *sy_org1
          - *sy_org2
          - *sy_org3

  TestOrgsChannel:
    Consortium: SampleConsortium
    Application:
      <<: *ApplicationDefaults
      Organizations:
        - *sy_org1
        - *sy_org2
        - *sy_org3


```

2. 生成系统创世区块  
`configtxgen -profile TestOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block`

3. 生成账本创世区块
`configtxgen -profile TestOrgsChannel -outputCreateChannelTx ./channel-artifacts/mychannel.tx -channelID mychannel`

4. 生成锚点文件(可选，每个组织生成一个，对应不同的channel要再次生成)
`configtxgen -profile TestOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org1MSPanchors.tx
 -channelID mychannel -asOrg Org1MSP`
# 